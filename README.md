# clusterProfiler

The clusterProfiler package implements methods to analyze and visualize
    functional profiles (GO and KEGG) of gene and gene clusters.


To install or update, run:

	source("http://bioconductor.org/biocLite.R")
	biocLite("clusterProfiler")
	

# Development

To install the development version of clusterProfiler, run:

	install.packages("clusterProfiler", repos="http://www.bioconductor.org/packages/devel/bioc", type="source")
	

# Citation

Please cite the following articles when using clusterProfiler. 
<http://online.liebertpub.com/doi/abs/10.1089/omi.2011.0118>

	G Yu, LG Wang, Y Han, QY He. 
	clusterProfiler: an R package for comparing biological themes among gene clusters.
	OMICS: A Journal of Integrative Biology. 2012, 16(5), 284-287.
	